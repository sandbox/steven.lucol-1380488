<?php

class Mic {

  private $filename;
  private $fid;
  private $nid;
  private $vid;
  private $uri;
  private $style_name;
  private $coords;
  private $settings;

  function __construct() {
    $opt['target'] = 'default';
    $opt['fetch'] = 'PDO::FETCH_OBJ';
    $opt['return'] = Database::RETURN_STATEMENT;
    $opt['throw_exception'] = TRUE;
    $this->settings['sql']['opt'] = $opt;
  }

  public function getFilename() {
    return $this->filename;
  }

  public function setFilename($filename) {
    $this->filename = $filename;
  }

  public function getFid() {
    return $this->fid;
  }

  public function setFid($fid) {
    $this->fid = $fid;
  }

  public function getNid() {
    return $this->nid;
  }

  public function setNid($nid) {
    $this->nid = $nid;
  }

  public function getVid() {
    return $this->vid;
  }

  public function setVid($vid) {
    $this->vid = $vid;
  }

  public function getUri() {
    return $this->uri;
  }

  public function setUri($uri) {
    return $this->uri = $uri;
  }

  public function getCoords() {
    return $this->crop;
  }

  public function setCoords($coords) {
    $this->coords = $coords;
  }

  public function getStyleName() {
    return $this->style_name;
  }

  public function setStyleName($style_name) {
    $this->style_name = $style_name;
  }

  public function getSettings() {
    return $this->settings;
  }

  public function setSettings($settings) {
    $this->settings = array_merge($this->settings, $settings);
  }

  public function select($variables) {
    
  }

  public function create($variables) {
    
  }

  public function update($variables) {
    
  }

  public function delele($variables = NULL) {
    $opt['target'] = 'default';
    $opt['fetch'] = 'PDO::FETCH_OBJ';
    $opt['return'] = Database::RETURN_STATEMENT;
    $opt['throw_exception'] = TRUE;

    $res = db_delete('mic', $opt)
            ->condition('nid', $this->nid, '=')
            ->condition('vid', $this->vid, '=')
            ->condition('fid', $this->fid, '=')
            ->execute();
  }

  public function initField(&$variables) {
    $this->nid = $variables['nid'];
    $this->vid = $variables['vid'];
    $this->style_name = $variables['style_name'];

    $opt = $this->settings['sql']['opt'];

    $result = db_select('mic', 'mic', $opt)
            ->fields('mic', array('crop', 'style_name', 'uri', 'fid'))
            ->condition('nid', $this->nid, '=')
            ->condition('vid', $this->vid, '=')
            ->condition('style_name', $this->style_name, '=')
            ->execute()
            ->fetchObject();

    if (!empty($result) && isset($result)) {
      $this->fid = $result->fid;
      $this->uri = $result->uri;
      $this->coords = json_decode($result->crop);
    }

    $image_info = $this->imageGetInfo($variables['element']['#default_value'], 'mic');

    $flag = (is_object($this->coords) && !is_NULL($result->crop));

    $style_effect_data = $this->imageGetStyleEffectData($variables['style_name']);

    $vars = array(
        'width' => $style_effect_data['width'],
        'height' => $style_effect_data['height'],
        'new_width' => $image_info['width'],
        'new_height' => $image_info['height'],
    );

    $dimensions = $this->computeAspectRatio($vars);

    $settings['mic_image'][$variables['style_name']] = array(
        'x' => isset($this->coords) && $this->coords->x > 0 ? $this->coords->x : 0,
        'y' => isset($this->coords) && $this->coords->y > 0 ? $this->coords->y : 0,
        'x2' => isset($this->coords) && $this->coords->x2 > 0 ? $this->coords->x2 : $dimensions['width'],
        'y2' => isset($this->coords) && $this->coords->y2 > 0 ? $this->coords->y2 : $dimensions['height'],
        'w' => isset($this->coords) && $this->coords->w > 0 ? $this->coords->w : $style_effect_data['width'],
        'h' => isset($this->coords) && $this->coords->h > 0 ? $this->coords->h : $style_effect_data['height'],
    );

    $w = $settings['mic_image'][$variables['style_name']]['w'];
    $h = $settings['mic_image'][$variables['style_name']]['h'];

    $ratios = $this->aspectRatio(array('width' => $w, 'height' => $h));

    $apr = $ratios['w'] . ':' . $ratios['h'];

    $settings['mic_image'][$variables['style_name']]['apr'] = $apr;

    drupal_add_js($settings, 'setting');
  }

  public function aspectRatio($variables) {
    $ratio['w'] = ceil($variables['width'] / $variables['height']);
    $ratio['h'] = ceil($variables['height'] / $variables['width']);
    return $ratio;
  }

  public function computeAspectRatio($variables) {
    $dimensions_x = array('width' => $variables['new_width'], 'height' => round($variables['height'] / $variables['width'] * $variables['new_width']));
    $dimensions_y = array('width' => round($variables['width'] / $variables['height'] * $variables['new_height']), 'height' => $variables['new_height']);
    if ($variables['new_width'] < $variables['new_height']) {
      if ($dimensions_x['width'] > $variables['new_width'])
        return $dimensions_y;
      return $dimensions_x;
    }
    elseif ($dimensions_y['width'] > $variables['new_width'])
      return $dimensions_x;
    return $dimensions_y;
  }

  public function imageGetInfo($fid, $style_name = NULL) {
    if ($file = file_load($fid)) {
      $header = file_get_content_headers($file);
      $url = $style_name ? image_style_url($style_name, $file->uri) : file_create_url($file->uri);
      // We ensure that the image is created in requesting it
      drupal_http_request($url, $header);
      $filepath = $style_name ? image_style_path($style_name, $file->uri) : file_create_path($file->uri);
      return image_get_info(drupal_realpath($filepath));
    }
    return NULL;
  }

  function imageGetStyleEffectData($style_name) {
    $styles = image_styles();
    if ($styles[$style_name]) {
      $style_effect_data = array_pop($styles[$style_name]['effects']);
      $style_effect_data = $style_effect_data['data'];
      return $style_effect_data;
    }
    return NULL;
  }

  private function generateUri($variables) {
    $n = node_load($variables['nid']);
    return $variables['element']['#upload_location'] . '/' . $variables['style_name'] . '/' . format_date($n->created, 'custom', 'Y/m/d/H/i/s/');
  }

  private function generateImage(&$variables) {
    $image = image_load(drupal_realpath($variables['element']['#file']->uri));
    $image_info = $this->imageGetInfo($variables['element']['#file']->fid, 'mic');
    $base = ($image->info['width'] < $image->info['height']) ? $image->info['width'] / $image_info['width'] : $image->info['height'] / $image_info['height'];
    $coords = json_decode($variables['crop']);
    image_crop($image, round($base * $coords->x), round($base * $coords->y), $base * $coords->w, $base * $coords->h);
    $image_info = $this->imageGetInfo($variables['element']['#file']->fid, $variables['style_name']);
    image_scale($image, $image_info['width'], $image_info['height'], TRUE);
    $uri = $this->generateUri($variables);
    if (!file_prepare_directory($uri))
      drupal_mkdir($uri, NULL, TRUE);
    $uri = $uri . $variables['element']['#file']->filename;
    if (image_save($image, $uri)) {
      $message = filter_xss('<b>[' . $variables['style_name'] . ']</b> ' . " created at => " . $uri);
      drupal_set_message($message);
      $variables['uri'] = $uri;
    }
  }

  public function fieldSaveDatas($element, $params) {
    if (isset($params)) {
      $options['fid'] = $params['fid'];
      $options['style_name'] = $params['style_name'];
      $options['nid'] = $params['nid'];
      $options['vid'] = $params['vid'];
      isset($params['coords']) ? $options['crop'] = json_encode($params['coords']) : NULL;
      if (isset($element['#file']) && $element['#file'] !== FALSE) {
        $variables = array_merge(array('element' => $element), $options);
        $this->generateImage($variables);
        $options['uri'] = $variables['uri'];
      }
      else {
        $options['uri'] = NULL;
        $options['crop'] = NULL;
      }

      $opt = $this->settings['sql']['opt'];
      $opt['throw_exception'] = FALSE;
      if (!db_insert('mic', $opt)->fields($options)->execute()) {
        $res = db_update('mic', array('fid' => $options['fid']))
                ->fields($options)
                ->condition('nid', $options['nid'], '=')
                ->condition('vid', $options['vid'], '=')
                ->condition('style_name', $options['style_name'], '=')
                ->execute();
      }
    }
  }

}
