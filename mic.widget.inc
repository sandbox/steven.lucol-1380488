<?php

/**
 * @file
 * Implement an widget for image field
 */

/**
 * Implements hook_field_widget_info().
 */
function mic_field_widget_info() {
  return array(
      'mic_image' => array(
          'label' => t('M.I.C'),
          'field types' => array('image'),
          'settings' => array(
              'progress_indicator' => 'throbber',
              'path' => 'mic',
          ),
          'behaviors' => array(
              'multiple values' => FIELD_BEHAVIOR_DEFAULT,
              'default value' => FIELD_BEHAVIOR_NONE,
          ),
      ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function mic_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];
  // Use the image widget settings form.
  $form = image_field_widget_settings_form($field, $instance);
  $form['list_style_name'] = array(
      '#type' => 'select',
      '#title' => 'Select on or more style to handle',
      '#options' => image_style_options(FALSE),
      '#default_value' => isset($settings['list_style_name']) ? $settings['list_style_name'] : 0,
      '#multiple' => TRUE,
      '#empty' => t('No items available'),
  );
  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function mic_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $elements = image_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $widget = $instance['widget'];
  $settings = $widget['settings'];
  $settings['nid'] = $form['nid']['#value'];
  $settings['vid'] = $form['vid']['#value'];
  $elements[$delta]["#title"] = t("original");
  $elements[$delta]['#attributes'] = array('class' => 'mic-field');
  $form_state["mic_field"] = $element["#field_name"];
  $elements[$delta][MODULE_NAME] = $elements[$delta]['#default_value']['fid'] ? module_invoke_all("mic_get_children", $elements[$delta], $settings) : array();
  return $elements[$delta];
}

/**
 * An element #value callback for custom file managed field #type.
 *
 * Sets the #default_value
 */
function mic_field_widget_value($element, $input = FALSE, $form_state) {
  $return = file_managed_file_value($element, $input, $form_state);

  $opt['target'] = 'default';
  $opt['fetch'] = 'PDO::FETCH_OBJ';
  $opt['return'] = Database::RETURN_STATEMENT;
  $opt['throw_exception'] = TRUE;

  $options['nid'] = $form_state['node']->nid;
  $options['vid'] = $form_state['node']->vid;
  $options['fid'] = $return['fid'];
  $options['style_name'] = $element['#title'];

  $result = db_select('mic', 'mic', $opt)
          ->fields('mic', array('crop', 'style_name', 'uri'))
          ->condition('nid', $options['nid'], '=')
          ->condition('vid', $options['vid'], '=')
          ->condition('fid', $options['fid'], '=')
          ->condition('style_name', $options['style_name'], '=')
          ->execute()
          ->fetchObject();
  // We get the coords of the element from database 
  $coords = !empty($result->crop) ? json_decode($result->crop) : NULL;
  $mic = new Mic();
  $style_effect_data = $style_effect_data = $mic->imageGetStyleEffectData($options['style_name']);
  $return += array(
      "x" => $coords ? $coords->x : 0,
      "y" => $coords ? $coords->y : 0,
      'x2' => isset($coords->x2) && $coords->x2 > 0 ? $coords->x2 : $style_effect_data['width'],
      'y2' => isset($coords->y2) && $coords->y2 > 0 ? $coords->y2 : $style_effect_data['height'],
      "w" => isset($coords->w) && $coords->w > 0 ? $coords->w : $style_effect_data['width'],
      "h" => isset($coords->h) && $coords->h > 0 ? $coords->h : $style_effect_data['height'],
      "uri" => isset($result->uri) ? $result->uri : NULL,
  );
  return $return;
}

/**
 * An element #process callback for the file_generic field type.
 *
 * Expands the file_generic type to include js settings and display fields.
 */
function mic_field_widget_process($element, &$form_state, $form) {
  if ($element['#file']) {
    $style_name = $element['#title'];

    $mic = new Mic();
    $image_info = $mic->imageGetInfo($element['#file']->fid, $style_name);

    $image_info2 = $mic->imageGetInfo($element['#file']->fid, 'mic');

    empty($element['#value']['uri']) ? $element['#value']['uri'] = image_style_url('mic', $element['#file']->uri) : '';

    $style_effect_data = $mic->imageGetStyleEffectData($style_name);
    $settings['mic_image'][$style_name] = array(
        'uri' => file_create_url($element['#file']->uri),
        'mic_width' => $image_info2['width'],
        'mic_height' => $image_info2['height'],
        'style_effect_width' => $style_effect_data['width'],
        'style_effect_height' => $style_effect_data['height'],
    );

    drupal_add_js($settings, 'setting');

    $element['x2']['#default_value'] = $element['#value']['x2'];
    $element['y2']['#default_value'] = $element['#value']['y2'];
    $element['w']['#default_value'] = $element['#value']['w'];
    $element['h']['#default_value'] = $element['#value']['h'];

    unset($element['filename']);

    $image_info = array();
    $image_info = image_get_info($element['#file']->uri);

    $element['view'] = array(
        '#type' => 'markup',
        '#markup' => theme('image', array('path' => image_style_url('mic', $element['#file']->uri), 'attributes' => array('class' => 'view'))),
        '#weight' => -50,
    );
  }
  return $element;
}

/*
 * Implements hook_mic_get_children()
 */

function mic_mic_get_children($element, $settings) {
  $element["mic"] = array('#type' => 'fieldset', '#title' => 'Styles');
  foreach ($settings['list_style_name'] as $style) {
    $options['nid'] = $settings['nid'];
    $options['vid'] = $settings['vid'];
    $options['style_name'] = $style;

    $opt['target'] = 'default';
    $opt['fetch'] = 'PDO::FETCH_OBJ';
    $opt['return'] = Database::RETURN_STATEMENT;
    $opt['throw_exception'] = TRUE;

    $result = db_select('mic', 'mic', $opt)
            ->fields('mic', array('fid', 'crop'))
            ->condition('nid', $options['nid'], '=')
            ->condition('vid', $options['vid'], '=')
            ->condition('style_name', $options['style_name'], '=')
            ->execute()
            ->fetchObject();

    $coords = !empty($result->crop) ? json_decode($result->crop) : NULL;

    $e = array();
    $e["#title"] = $style;
    $e["#type"] = 'managed_file';
    $e["#attributes"] = array('class' => $style . ' mic-' . $style);
    $e['#default_value'] = (isset($result->fid) && $result->fid > 0) ? $result->fid : $element['#default_value']['fid'];
    $element_info = element_info('managed_file');
    $e['#value_callback'] = 'mic_field_widget_value';
    $e['#process'] = array_merge($element_info['#process'], array('mic_field_widget_process'));
    $e['#field_parents'] = array('mic');
    $e['#upload_location'] = $element['#upload_location'] . $settings['path'];
    $e['#upload_validators'] = $element['#upload_validators'];
    $e['x'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->x : 0, '#attributes' => array('id' => 'mic-coords-x-' . $style, 'style_name' => $style));
    $e['y'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->y : 0, '#attributes' => array('id' => 'mic-coords-y-' . $style, 'style_name' => $style));
    $e['x2'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->x2 : 0, '#attributes' => array('id' => 'mic-coords-x2-' . $style, 'style_name' => $style));
    $e['y2'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->y2 : 0, '#attributes' => array('id' => 'mic-coords-y2-' . $style, 'style_name' => $style));
    $e['w'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->w : 0, '#attributes' => array('id' => 'mic-coords-w-' . $style, 'style_name' => $style));
    $e['h'] = array('#type' => 'hidden', '#default_value' => $coords ? $coords->h : 0, '#attributes' => array('id' => 'mic-coords-h-' . $style, 'style_name' => $style));

    $element["mic"][$e["#title"]] = $e;
  }
  return $element["mic"];
}

/*
 * Implements hook_mic_field_save_datas()
 */

function mic_mic_field_save_datas($element, $params) {
  $mics = variable_get('mic_' . $params['nid']);
  return $mics[$params['style_name']]->fieldSaveDatas($element, $params);
}

/**
 * Implements hook_field_formatter_info().
 */
function mic_field_formatter_info() {
  $formatters = array(
      'mic' => array(
          'label' => t('M.I.C'),
          'field types' => array('image'),
          'settings' => array('image_style' => '', 'image_link' => ''),
      ),
  );
  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function mic_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = image_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  $result = db_select('field_config_instance', 'fci')
          ->fields('fci', array('data'))
          ->condition('field_name', $instance['field_name'], '=')
          ->execute()
          ->fetchObject();
  $data = unserialize($result->data);
  $element['image_style']['#options'] = $data['widget']['settings']['list_style_name'];
  return $element;
}

/**
 * Implements hook_field_formatter_view().
 */
function mic_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = image_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
  foreach ($items as $delta => $item) {
    $element[$delta]['#theme'] = 'mic_formatter';
    $element[$delta]['#item'] = array_merge($item, array('entity' => $entity));
  }
  return $element;
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: An array of image data.
 *   - image_style: An optional image style.
 *   - path: An array containing the link 'path' and link 'options'.
 *   - nid: node id.
 *   - vid: version node id.
 *
 * @ingroup themeable
 */
function theme_mic_formatter($variables) {
  $item = $variables['item'];
  $image = array(
      'path' => $item['uri'],
      'alt' => $item['alt'],
      'nid' => $item['entity']->nid,
      'vid' => $item['entity']->vid,
  );

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

// Do not output an empty 'title' attribute.
  if (drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  if ($variables['image_style']) {
    unset($image['width']);
    unset($image['height']);
    $image['fid'] = $item['fid'];
    $image['style_name'] = $variables['image_style'];
    $output = theme('mic_image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  if (!empty($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = $variables['path']['options'];
// When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - style_name: style name.
 *   - nid: node id.
 *   - vid: version node id.
 *
 * @ingroup themeable
 */
function theme_mic_image_style($variables) {
  $opt['target'] = 'default';
  $opt['fetch'] = 'PDO::FETCH_OBJ';
  $opt['return'] = Database::RETURN_STATEMENT;
  $opt['throw_exception'] = TRUE;

  $result = db_select('mic', 'mic', $opt)
          ->fields('mic', array('uri'))
          ->condition('nid', $variables['nid'], '=')
          ->condition('vid', $variables['vid'], '=')
          ->condition('style_name', $variables['style_name'], '=')
          ->execute()
          ->fetchObject();

  $variables['path'] = $result->uri;
  return theme('image', $variables);
}
