f
/**
 * @file
 * Provides JavaScript additions to the managed file field type.
 *
 * This file provides progress bar support (if available), popup windows for
 * file previews, and disabling of other file fields during Ajax uploads (which
 * prevents separate file fields from accidentally uploading files).
 */

(function ($) {

  /**
 * Attach behaviors to the file upload and remove buttons.
 */
  Drupal.behaviors.micCropping = {
    attach: function (context, settings) {
      
      $(document).ready(function(){
        
        
    
      $.each(settings.mic_image, function(field_name) {
        var element = settings.mic_image[field_name];
          
        element.uri = $.isArray(element.uri) ? element.uri.pop() : element.uri;
        element.style_effect_width = $.isArray(element.style_effect_width) ? element.style_effect_width.pop() : element.style_effect_width;
        element.style_effect_height = $.isArray(element.style_effect_height) ? element.style_effect_height.pop() : element.style_effect_height;
        element.mic_width = $.isArray(element.mic_width) ? element.mic_width.pop() : element.mic_width;
        element.mic_height = $.isArray(element.mic_height) ? element.mic_height.pop() : element.mic_height;
      
          
        if ($('#mic-preview-'+field_name).length <= 0)
        {
      
          //console.log(field_name);
          $('<div><img class="mic-preview" id="mic-preview-'+field_name+'" src="'+element.uri+'" style="position: relative;" /><div>')
          .css({
            //float: 'left',
            position: 'relative',
            overflow: 'hidden',
            width: element.style_effect_width,
            height: element.style_effect_height
          })
          .insertAfter('.mic-'+field_name+' img');
          
        }
          
        // $('#mic-preview-'+field_name).attr('src', element.uri)
      
       console.log($('.mic-'+field_name+' img.view').parent());
       $('.mic-'+field_name+' img.view').imgAreaSelect({
          handles: true,
          aspectRatio: element.apr,
          onInit: Drupal.behaviors.micCropping.init,
          onSelectChange: Drupal.behaviors.micCropping.preview,
          onSelectEnd: Drupal.behaviors.micCropping.preview,
          x1: element.x, 
          y1: element.y, 
          x2: element.x2, 
          y2: element.y2,
          width: element.w, 
          height: element.h,
          keys: true,
          enable: true,
          zIndex: 1000 
        }); 
           
      });
      
     });
    },
    detach: function (context, settings, trigger) {
       
      $.each(settings.mic_image, function(field_name) {
        //console.log(settings.mic_image[field_name]);
        // var element = settings.mic_image[field_name];
        //console.log(element);
        $('.mic-preview').parent().remove();
        //$('img.view').remove();
        $('img.view', context).imgAreaSelect({
          remove: true
        });

      //settings.mic_image = null;
      });
    //  console.log($(context));
     
 
    },
    getCoords: function(obj, c) {
      var style_name = $(obj).parent().attr('class').split(' ', 1)
      style_name = style_name[0];
      //console.log(obj);
      var input_name_x = 'mic-coords-x-' + style_name;
      var input_name_y = 'mic-coords-y-' + style_name;
      var input_name_x2 = 'mic-coords-x2-' + style_name;
      var input_name_y2 = 'mic-coords-y2-' + style_name;        
      var input_name_w = 'mic-coords-w-' + style_name;
      var input_name_h = 'mic-coords-h-' + style_name;

      $('input[id="'+input_name_x+'"]').val(c.x1 ? c.x1 : 0);
      $('input[id="'+input_name_y+'"]').val(c.y1 ? c.y1 : 0);
      $('input[id="'+input_name_x2+'"]').val(c.x2 ? c.x2 : 0);
      $('input[id="'+input_name_y2+'"]').val(c.y2 ? c.y2 : 0);        
      $('input[id="'+input_name_w+'"]').val(c.width ? c.width : 0);
      $('input[id="'+input_name_h+'"]').val(c.height ? c.height : 0);
    },
    preview: function(obj, c) {
      Drupal.behaviors.micCropping.getCoords(obj, c);
      var id = $(obj).parent().children('.mic-preview').attr('id');
      var style_name = $(obj).parent().attr('class').split(' ', 1);
      style_name = style_name[0];     
      
      var element = Drupal.settings.mic_image[style_name];
      element.uri = $.isArray(element.uri) ? element.uri.pop() : element.uri;
      element.style_effect_width = $.isArray(element.style_effect_width) ? element.style_effect_width.pop() : element.style_effect_width;
      element.style_effect_height = $.isArray(element.style_effect_height) ? element.style_effect_height.pop() : element.style_effect_height;
      element.mic_width = $.isArray(element.mic_width) ? element.mic_width.pop() : element.mic_width;
      element.mic_height = $.isArray(element.mic_height) ? element.mic_height.pop() : element.mic_height;
    
      var scaleX = element.style_effect_width  / (c.width || 1);
      var scaleY = element.style_effect_height / (c.height || 1);

      var width = element.mic_width;
      var height = element.mic_height;
      
      // console.log(c);
      $('#mic-preview-' + style_name).css({
        width: Math.round(scaleX * width) + 'px',
        height: Math.round(scaleY * height) + 'px',
        marginLeft: '-' + Math.round(scaleX * c.x1) + 'px',
        marginTop: '-' + Math.round(scaleY * c.y1) + 'px'
      });
    
    }, 
    
    init: function(obj, c) {
      Drupal.behaviors.micCropping.preview(obj, c);
      console.log("init");
    }    
  };


})(jQuery);